/*
    SPC5 RLA - Copyright (C) 2015 STMicroelectronics

    Licensed under the Apache License, Version 2.0 (the "License").
    You may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/* Inclusion of the main header files of all the imported components in the
   order specified in the application wizard. The file is generated
   automatically.*/
#include "components.h"
#include "serial_lld_cfg.h"

volatile uint8_t rxbuf;

void cb_uart0_rx(SerialDriver *sdp)
{
	rxbuf = *sdp->rx_buf;
	pal_lld_togglepad(PORT_C, PC_LED8);
}
void cb_uart0_tx(SerialDriver *sdp)
{
	(void)sdp;
}
/*
 * Application entry point.
 */
int main(void) {

  //uint8_t message[]= "A";
  uint8_t rx_buf;
  /* Initialization of all the imported components in the order specified in
     the application wizard. The function is generated automatically.*/
  componentsInit();

  /* Enable Interrupts */
  irqIsrEnable();

  /*
   * Activates the serial driver 1 using the driver default configuration.
   */
  sd_lld_start(&SD1, &serial_config_configuration_name);
  SD1.rx_buf = &rx_buf;

  /* Application main loop.*/
  for ( ; ; ) {
    if(rxbuf == 'A')
    {
    	pal_lld_setpad(PORT_C, PC_LED7);
    }
    else if(rxbuf == 'B')
    {
    	pal_lld_clearpad(PORT_C, PC_LED7);
    }
  }
}

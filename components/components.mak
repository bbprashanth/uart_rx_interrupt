# List components imported by the makefile.
# This is an automatically generated file, do not edit manually.

include ./components/spc560bxx_platform_component_rla/mak/component.mak
include ./components/spc560bxx_init_package_component_rla/mak/component.mak
include ./components/spc560bxx_low_level_drivers_component_rla/mak/component.mak
include ./components/spc560bxx_board_initialization_component_rla/mak/component.mak
include ./components/spc560bxx_clock_component_rla/mak/component.mak
include ./components/spc560bxx_irq_component_rla/mak/component.mak
include ./components/spc560bxx_osal_component_rla/mak/component.mak
